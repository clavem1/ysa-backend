(function() {
  module.exports = function(app) {
    const Ctrl = require("./talents.controller")();
    app
      .route("/talents")
      .get(Ctrl.list)
      .post(Ctrl.create);
    app
      .route("/talents/:id")
      .get(Ctrl.read)
      .put(Ctrl.update)
      .delete(Ctrl.delete);
    app.route("/upload").post(Ctrl.upload);
  };
})();
