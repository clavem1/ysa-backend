(function() {
  const Talents = require("./talents.model").talentModel;
  const multer = require("multer"),
    fs = require("fs"),
    uploadFileService = require("../../utils/uploadFileService");
  module.exports = function() {
    return {
      list: function(req, res) {
        Talents.find({}, { password: 0 }, (error, list) => {
          if (error) {
            res.status(500).json({
              status: "error",
              body: error
            });
          } else {
            res.status(200).json({
              status: "success",
              body: list
            });
          }
        });
      },
      create: function(req, res) {
        var talent = new Talents(req.body);
        talent.save((error, rep) => {
          if (error) {
            res.status(500).json({
              status: "error",
              body: error
            });
          } else {
            res.status(200).json({
              status: "success",
              body: rep
            });
          }
        });
      },
      read: function(req, res) {
        Talents.findOne({ _id: req.params.id }, function(error, talent) {
          if (error) {
            res.status(500).json({
              status: "error",
              body: error
            });
          } else if (!talent) {
            res.status(404).json({
              status: "not found",
              body: talent
            });
          } else {
            res.status(200).json({
              status: "success",
              body: talent
            });
          }
        });
      },
      update: function(req, res) {
        Talents.findOneAndUpdate(
          { _id: req.params.id },
          req.body,
          { new: true },
          function(error, talent) {
            if (error) {
              res.status(500).json({
                status: "error",
                body: error
              });
            } else {
              res.status(200).json({
                status: "success",
                body: talent
              });
            }
          }
        );
      },
      delete: function(req, res) {
        Talents.deleteOne({ _id: req.params.id }, function(error, rep) {
          if (error) {
            res.status(500).json({
              status: "error",
              body: error
            });
          } else {
            res.status(201).end();
          }
        });
      },
      upload: async function(req, res) {
        var upload = multer({ dest: "public/fichiers" }).single("fichier");
        try {
          await uploadFileService.uploadFile(upload, req, res);
        } catch (err) {
          return res.json(err);
        }

        let filename = req.file.originalname.split(".");
        let extension = filename[filename.length - 1];
        fs.rename(req.file.path, req.file.path + "." + extension, function(
          err
        ) {
          res.json(req.file);
        });
      }
    };
  };
})();
