const Talents = require("./talents.model").talentModel;
const abstractService = require("./../abstract/abstract.service")(Talents);

module.exports = {
  ...abstractService
};
