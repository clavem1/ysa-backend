(function() {
  var mongoose = require("mongoose");
  var Schema = mongoose.Schema;
  mongoose.set("useCreateIndex", true);
  var talentSchema = new Schema({
    nom: { type: String, required: true },
    prenom: { type: String, required: true },
    dateNaissance: { type: Date, required: false },
    profile: { type: mongoose.Schema.Types.ObjectId, ref: "Profiles" }
  });

  module.exports = {
    talentModel: mongoose.model("Talents", talentSchema)
  };
})();
