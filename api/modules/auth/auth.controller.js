(function() {
  const profileService = require("../profile/profile.service");
  const talentService = require("../talents/talents.service");
  const Profile = require("./../profile/profile.model").profileModel;
  const bcrypt = require("bcrypt");
  const saltRounds = 10,
    jwt = require("jsonwebtoken");
  module.exports = function() {
    return {
      signin: async (req, res) => {
        let profile = await Profile.findOne({ email: req.body.email });
        if (!profile)
          return res.json({
            status: "error",
            body: "email incorrecte"
          });
        bcrypt
          .compare(req.body.password, profile.password)
          .then(function(correct) {
            if (correct) {
              let reponse = {
                _id: profile._id,
                username: profile.email,
                role: profile.role
              };
              let token = jwt.sign(
                {
                  data: reponse
                },
                "secret",
                { expiresIn: 60 * 60 }
              );
              reponse.token = token;
              return res.json({
                status: "success",
                body: reponse
              });
            } else {
              return res.json({
                status: "error",
                body: "Mot de passe incorrecte"
              });
            }
          });
      },
      signup: (req, res) => {
        bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
          if (err) {
            console.log(err);
          } else {
            let profileData = {
              password: hash,
              email: req.body.email,
              role: req.body.role
            };
            profileService
              .save(profileData)
              .then(profile => {
                if (profile.role == "TALENT") {
                  talentService
                    .save({
                      prenom: req.body.prenom,
                      nom: req.body.nom,
                      dataNaissance: req.body.dateNaissance,
                      profile: profile
                    })
                    .then(talent => {
                      return res.json({
                        body: talent,
                        status: "success"
                      });
                    })
                    .catch(error => {
                      console.log(error);
                      return res.json({
                        status: "error",
                        body: err
                      });
                    });
                }
              })
              .catch(error => {
                return res.json({
                  status: "error",
                  body: err
                });
              });
          }
        });
      }
    };
  };
})();
