(function() {
  var mongoose = require("mongoose");
  var Schema = mongoose.Schema;
  mongoose.set("useCreateIndex", true);
  var profileSchema = new Schema({
    email: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    role: { type: String, required: true, default: "talent" }
  });

  module.exports = {
    profileModel: mongoose.model("Profiles", profileSchema)
  };
})();
