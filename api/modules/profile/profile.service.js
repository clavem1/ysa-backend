const Profile = require("./profile.model").profileModel;
const abstractService = require("./../abstract/abstract.service")(Profile);

module.exports = {
  ...abstractService
};
