module.exports = function(Model) {
  return {
    save: function(data) {
      return new Promise((resolve, reject) => {
        model = new Model(data);
        model.save(data, (error, savedData) => {
          if (error) reject(error);
          else resolve(savedData);
        });
      });
    }
  };
};
