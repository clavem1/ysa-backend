(function() {
  module.exports = function(Model) {
    return {
      list: function(req, res) {
        Model.find({}, (error, list) => {
          if (error) {
            res.status(500).json({
              status: "error",
              body: error
            });
          } else {
            res.status(200).json({
              status: "success",
              body: list
            });
          }
        });
      },
      create: function(req, res) {
        var model = new Model(req.body);
        model.save((error, rep) => {
          if (error) {
            res.status(500).json({
              status: "error",
              body: error
            });
          } else {
            res.status(200).json({
              status: "success",
              body: rep
            });
          }
        });
      },
      read: function(req, res) {
        Model.findOne({ _id: req.params.id }, function(error, model) {
          if (error) {
            res.status(500).json({
              status: "error",
              body: error
            });
          } else if (!model) {
            res.status(404).json({
              status: "not found",
              body: model
            });
          } else {
            res.status(200).json({
              status: "success",
              body: model
            });
          }
        });
      },
      update: function(req, res) {
        Model.findOneAndUpdate(
          { _id: req.params.id },
          req.body,
          { new: true },
          function(error, model) {
            if (error) {
              res.status(500).json({
                status: "error",
                body: error
              });
            } else {
              res.status(200).json({
                status: "success",
                body: model
              });
            }
          }
        );
      },
      delete: function(req, res) {
        Model.deleteOne({ _id: req.params.id }, function(error, rep) {
          if (error) {
            res.status(500).json({
              status: "error",
              body: error
            });
          } else {
            res.status(201).end();
          }
        });
      }
    };
  };
})();
