(function() {
  module.exports = function(app, Model, modelName) {
    const Ctrl = require("./abstract.controller")(Model);
    app
      .route(`/${modelName}`)
      .get(Ctrl.list)
      .post(Ctrl.create);
    app
      .route(`/${modelName}/:id`)
      .get(Ctrl.read)
      .put(Ctrl.update)
      .delete(Ctrl.delete);
  };
})();
